package servlets;

import java.io.IOException;
import java.lang.Math;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/form")
public class CreditServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException 
	{
		String amount = request.getParameter("amount");
		String installments = request.getParameter("installments");
		String percentage = request.getParameter("percentage");
		String type = request.getParameter("type");
		
		if(amount==null || amount.equals("")|| type.isEmpty())
		{
			response.sendRedirect("/");
		}
		if(installments==null || installments.equals("")|| type.isEmpty())
		{
			response.sendRedirect("/");
		}
		if(percentage==null || percentage.equals("")|| type.isEmpty())
		{
			response.sendRedirect("/");
		}
		if(type==null || type.equals("") || type.isEmpty())
		{
			response.sendRedirect("/");
		}
		
	
		double S = Double.parseDouble(amount);
		double n = Double.parseDouble(installments);
		double r = Double.parseDouble(percentage);
		double installment;
		double interest;
		double asset;

		

		r = r / 100;
		double q = 1 + (r/12);
		if (type.equals("const"))
		{
		installment = S * Math.pow(q, n) * (q-1)/(Math.pow(q, n)-1); 


		response.setContentType("text/html");
		response.getWriter().println("<table border='1'><tr><td>Nr</td><td>kapital</td><td>odsetki</td><td>ca�kowita kwota raty</td></tr>");
		for (int rateNumber = 1; rateNumber <= n; rateNumber++ )
		{
			interest = S*r/n;
			asset = installment - interest;
			S=S-asset;
		response.getWriter().println(
			"<td>" + rateNumber + "</td>" + 
		"<td>" + Math.round(asset*100d)/100d + "</td>"
				+ "<td>" + Math.round(interest*100.0)/100.0 +"</td>"
						+ "<td>" + Math.floor(installment*100.0)/100.0 + "</td>"
								+ "</tr>");
		}
		}
		
		if(type.equals("dec"))
		{
			installment = S * Math.pow(q, n) * (q-1)/(Math.pow(q, n)-1); 
			asset = S/12;

			response.setContentType("text/html");
			response.getWriter().println("<table border='1'><tr><td>Nr</td><td>kapital</td><td>odsetki</td><td>ca�kowita kwota raty</td></tr>");
			for (int rateNumber = 1; rateNumber <= n; rateNumber++ )
			{
				interest = S*r/n;
				S=S-asset;
				installment = interest+asset;
			response.getWriter().println(
				"<td>" + rateNumber + "</td>" + 
			"<td>" + Math.round(asset*100d)/100d + "</td>"
					+ "<td>" + Math.round(interest*100.0)/100.0 +"</td>"
							+ "<td>" + Math.floor(installment*100.0)/100.0 + "</td>"
									+ "</tr>");
			}
		}

		}
	
}
